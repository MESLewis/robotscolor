package main;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Random;

public class Main extends Applet implements Runnable{
	public static final int WINDOW_WIDTH = 700;
	public static final int WINDOW_HEIGHT = 500;
	
	public static Random rand = new Random();
	Image buffer;
	Graphics bg;
	Thread thread = new Thread(this);
	
	int maxRobots = 50;
	public static int tileSize = 5;
	public static int gridWidth = WINDOW_WIDTH/tileSize;
	public static int gridHeight = WINDOW_HEIGHT/tileSize;
	
	public static Color[][] colorGrid = new Color[gridWidth+1][gridHeight+1];
	
	ArrayList<Robot> robotList = new ArrayList<Robot>();
	
	public void init() {
		buffer = createImage(WINDOW_WIDTH, WINDOW_HEIGHT);
		bg = buffer.getGraphics();
		addRobots(maxRobots);
		
		
		
		
		thread.start();
	}
	
	public void addRobots(int robotsToAdd) {
		for(int i = 0; i < robotsToAdd; i++) {
			Robot robot = new Robot(rand.nextInt(gridWidth-1), rand.nextInt(gridHeight-1), new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256)), rand.nextInt(4));
			robotList.add(robot);
		}
	}
	
	public void paint(Graphics g) {
		//drawGrid(tileSize, bg);
		
		for(int i = 0; i < colorGrid.length-1; i++) {
			for(int j = 0; j < colorGrid[i].length-1; j++) {
				if(colorGrid[i][j] == null) {
					bg.setColor(Color.LIGHT_GRAY);
				} else {
					bg.setColor(colorGrid[i][j]);
				}
				bg.fillRect(i*tileSize, j*tileSize, tileSize, tileSize);
			}
		}
		
		for(Robot robot : robotList) {
				bg.setColor(robot.color.brighter());
				bg.fillRect(robot.x*tileSize, robot.y*tileSize, tileSize, tileSize);
		}
		
		g.drawImage(buffer, 0, 0, this);
	}
	
	public void drawGrid(int gridSize, Graphics g) {
		g.setColor(Color.green);
		for(int x = 0; x <= WINDOW_WIDTH; x += gridSize) {
			g.drawLine(x, 0, x, WINDOW_HEIGHT);
		}
		for(int y = 0; y <= WINDOW_HEIGHT; y += gridSize) {
			g.drawLine(0, y, WINDOW_WIDTH, y);
		}
	}
	
	public void update(Graphics g) {
		paint(g);
	}

	
	public void run() {
		while(true) {
			repaint();
			
			for(Robot robot : robotList) {
				robot.update();
				colorGrid[robot.x][robot.y] = robot.color;
			}
			
			
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void main(String[] args) {
		Main main = new Main();
		main.init();
	}

}
