package main;

import java.awt.Color;

public class Robot {
	int x;
	int y;
	int moveSpeedX = 1;
	int moveSpeedY = 1;
	Color color;
	
	int direction;     /*  	   0 	 *
						*  3 Robot 1 *
	 					*  	   2	 */

	public Robot(int x, int y, Color color, int direction) {
		this.x = x;
		this.y = y;
		this.color = color;
		this.direction = direction;
	}
	
	
	public void update() {
		if(Main.rand.nextInt(11) < 1) {
			turn();
		}
		
		
		forward();
		stayInBounds();
		avoidColorTrail();
		stayInBounds();//must be last.
	}
	
	//Does a weird checkerboard thing and doesn't really work to begin with.
	public void avoidColorTrail() {
		int i = 0;
		while(onColorTile()) {
			i++;
			
			this.turn();
			
			//Failed attempts at making it actually avoid color trails.
			//Provides much less entertaining results compared to just turning.
			//this.reverseDirection();
			//this.forward();
			//this.reverseDirection();//Backup and turn.
//			int testDirection = turnLeft(direction);
//			int[] testCoords = this.getForwardOne(x, y, testDirection);
//			
//			if(Main.colorGrid[testCoords[0]][testCoords[1]] == null) {
//				direction = testDirection;
//			}
			
			if(i >= 10) {
				//this.color = this.color.brighter();
				//direction = -1;
				break;
			}
		}
	}
	
	public void stayInBounds() {
		if(x < 0) {
			x = 0;
			turn();
		}
		if(y < 0) {
			y = 0;
			turn();
		}
		if(x > Main.gridWidth) {
			x = Main.gridWidth-1;
			turn();
		}
		if(y > Main.gridHeight) {
			y = Main.gridHeight-1;
			turn();
		}
	}
	
	public void forward() {
		int[] newCoords = getForwardOne(this.x, this.y, this.direction);
		this.x = newCoords[0];
		this.y = newCoords[1];
	}
	
	private int[] getForwardOne(int xGet, int yGet, int directionToGet) {
		int[] newCoords = new int[2];
		int newX = xGet;
		int newY = yGet;
		switch (direction) {
		case -1:
			//do nothing.
			break;
		case 0:
			newY = yGet - moveSpeedY;
			break;
		case 1:
			newX = xGet + moveSpeedX;
			break;
		case 2:
			newY = yGet + moveSpeedY;
			break;
		case 3:
			newX = xGet - moveSpeedX;
			break;
		default:
			direction = 0;
			break;
		}
		newCoords[0] = newX;
		newCoords[1] = newY;
		return newCoords;
	}
	
	public void turn() {
		if(direction >= 0) {
			switch(Main.rand.nextInt(3)) {
			case 0:
				direction = turnRight(direction);
				break;
			case 1:
				direction = turnLeft(direction);
				break;
			case 2:
				direction = reverseDirection(direction);
				break;
			}
		}
	}
	
	private int turnLeft(int startDirection) {
		int returnDirection = startDirection;
		returnDirection--;
		if(returnDirection < 0) {
			returnDirection = 3;
		}
		return returnDirection;
	}
	
	private int turnRight(int startDirection) {
		int returnDirection = startDirection;
		returnDirection++;
		if(returnDirection > 3) {
			returnDirection = 0;
		}
		return returnDirection;
	}
	
	public int reverseDirection(int startDirection) {
		int returnDirection = 2;
		switch(startDirection) {
		case 0:
			returnDirection = 2;
			break;				
		case 1:
			returnDirection = 3;
			break;
		case 2:
			returnDirection = 0;
			break;
		case 3:
			returnDirection = 1;
			break;
		default:
			returnDirection = 2;
			break;
		}
		return returnDirection;
	}
	
	public boolean onColorTile() {
		if(x >= 0 && y >= 0 && x < Main.gridWidth && y < Main.gridHeight) {
			if(Main.colorGrid[x][y] != null) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
	
	public int getClearDirection() {
		int clearDirection = -1;
		return clearDirection;
	}
	
	
	
}
